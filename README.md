# Precompiled Wheels of CLIPick

This repository is all about python wheels (*.whl) of CLIPick.

1. Find the right wheel for your environment. e.g., if you use Python 2.7.x on macOS 10.12, then download `CLIPick-0.1.0b1-cp27-cp27m-macosx_10_6_intel.whl`.
2. Run installation with `pip`.
    * If you want to install CLIPick for Python 2.7.x on macOS 10.6 or above, then type `pip2 install CLIPick-0.1.0b1-cp27-cp27m-macosx_10_6_intel.whl`
    * if you want to install CLIPick for Python 3.6.x on macOS 10.6 or above, then type `pip3 install CLIPick-0.1.0b1-cp36-cp36m-macosx_10_6_intel.whl`
3. **You need to install BedTools manually**. Installing from wheel does not installs BedTools. Please download and install BedTools following [the guides](http://bedtools.readthedocs.io/en/latest/content/installation.html).


# Available platforms (updating)

Below is a list of platforms and corresponding names of the wheels in this repository. If your platform is not listed below, please install the package from the source [[2.7.x](https://gitlab.com/CLIPick/CLIPick-package), [3.6.x](https://gitlab.com/CLIPick/CLIPick)].

|                   | **Python 2.7.x**                                   | **Python 3.6.x**                                   |
|:-----------------:|----------------------------------------------------|----------------------------------------------------|
| **macOS  (OS X)** | `CLIPick-0.1.0b1-cp27-cp27m-macosx_10_6_intel.whl` | `CLIPick-0.1.0b1-cp36-cp36m-macosx_10_6_intel.whl` |
|        **Ubuntu** | `CLIPick-0.1.0b1-cp27-cp27mu-linux_x86_64.whl`     | `CLIPick-0.1.0b1-cp36-cp36m-linux_x86_64.whl`      |




# Troubleshooting

* **Please upgrade `pip` to the latest version (`pip install --upgrade pip`).** It was reported that installing with `pip` version < 10.0.0 might cause an error.
* **Make sure that BedTools is installed before running CLIPick.** BedTools will not be installed automatically. Please download and install BedTools following the [guides](http://bedtools.readthedocs.io/en/latest/content/installation.html).
